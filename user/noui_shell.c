#include "noui_shell.h"
#include "commands.h"
#include "../shared/stdio.h"
#include "syscalls.h"
#include "../shared/string.h"
#include "mem.h"
#include "../shared/stddef.h"

extern command_def_t commands[];

void display_prompt() {
	char buf[50];
	get_hour_str(buf, 50);
	printf("%s nouishell_$ ", buf);
}

char *get_word(char **line, int length, int *strlen) {
	int i = 0;
	for(i = 0; i < length && (*line)[i] == ' '; i++);
	char *start_ptr = *line + i;
	for(*line = start_ptr; i < length && **line != ' '; *line = *line+1)
		i++;

	if (i == 0)
	{
		return NULL;
	}
	unsigned size = *line-start_ptr;
	if (**line == ' ' && i == length && (*line)[i] == ' ')
		size--;
	if (strlen != NULL)
		*strlen = size;
	char *word = mem_alloc(size+1);
	strncpy(word, start_ptr, size+1);
	word[size] = '\0';
	return word;
}

command_t *read_cmd() {
	char buffer[255];
	int cmd_len = cons_read(buffer, sizeof(buffer));
	if (cmd_len == 0)
		return NULL;
	char *buffer_ptr = buffer;

	command_t *cmd = mem_alloc(sizeof(command_t));
	cmd->name = get_word(&buffer_ptr, cmd_len, NULL);

	unsigned argcount = 0;

	while (argcount < sizeof(cmd->params.args) && buffer_ptr < (buffer+cmd_len))
	{
		cmd->params.args[argcount] = get_word(&buffer_ptr, (buffer+cmd_len) - buffer_ptr, &cmd->params.arg_length[argcount]);
		argcount++;
	}
	cmd->params.argcount = argcount;

	for(unsigned i = 0; i < get_command_count()/sizeof(command_def_t); i++)
	{
		if(strcmp(commands[i].name, cmd->name) == 0)
		{
			cmd->fct = commands[i].fct;
			break;
		}
	}

	return cmd;
}

void free_cmd(command_t *cmd) {
	int i = 0;
	while (i < cmd->params.argcount && cmd->params.args[i] != NULL)
	{
		mem_free(cmd->params.args[i], cmd->params.arg_length[i]+1);
		i++;
	}
	if ( cmd->name != NULL )
		mem_free(cmd->name, strlen(cmd->name)+1);
	mem_free(cmd, sizeof(command_t));
}

int noui_shell_start(void *arg) {
	(void) arg;
	while (1) {
		display_prompt();
		command_t *cmd = read_cmd();
		if (cmd != NULL)
		{
			if (cmd->fct != NULL)
			{
				waitpid(start(cmd->fct, 4096, 128, cmd->name, &cmd->params), NULL);
			}
			else
				printf("Unknown command: %s\n", cmd->name);
			free_cmd(cmd);
		}
	}
}
