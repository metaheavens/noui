//
// Created by metaheavens on 07/06/18.
//

#ifndef PSYS_BASE_SPRINTF_H
#define PSYS_BASE_SPRINTF_H

int sprintf(char *s, const char *fmt, ...);

#endif //PSYS_BASE_SPRINTF_H
