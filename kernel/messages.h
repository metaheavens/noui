#ifndef _MESSAGES_H
#define _MESSAGES_H

int pcreate(int count);

int preceive(int fid, int* msg);

int pdelete(int fid);

int pcount(int fid, int *count);

int psend(int fid, int message);

int preset(int fid);

void msgstate();

#endif //_MESSAGES_H
