#include "const.h"
#include "ecran.h"
#include "../kernel/cpu.h"
#include "string.h"
#include "kbd.h"
#include "../shared/stdint.h"
#include "../shared/string.h"
#include "../shared/printf.h"
#include "../shared/const.h"
#include "../shared/types.h"
#include "../shared/stddef.h"

#define process_add_waiting(p) queue_add(p, &waiting_process_queue, process_t, l_wait_io, priority)
#define process_pop_waiting(p) queue_out(&waiting_process_queue, process_t, l_wait_io)

#define process_add_waiting_key(p) queue_add(p, &waiting_key_process_queue, process_t, l_wait_io_key, priority)
#define process_pop_waiting_key(p) queue_out(&waiting_key_process_queue, process_t, l_wait_io_key)

static uint32_t cur_x = 0;
static uint32_t cur_y = 0;
uint32_t col_txt = 15;
uint32_t col_bg = 0;

char keyboard_buffer[1024];
uint32_t keyboard_buffer_index = 0;

LIST_HEAD(waiting_process_queue);
LIST_HEAD(waiting_key_process_queue);

unsigned int cons_echo_bool = 1;
/*uint16_t *ptr_mem(uint32_t lig, uint32_t col)
{
    return (uint16_t*)(MEM_VIDEO + 2 * (lig * 80 + col));
}*/

/*void ecrit_car(
        uint32_t lig, 
        uint32_t col, 
        char c, 
        uint32_t coul_texte, 
        uint32_t coul_fond
        )
{
    uint16_t *case_mem = ptr_mem(lig, col);
    char config_char = 0xFF;
    char color_txt_flags = coul_texte;
    char color_bg_flags = coul_fond << 3;
    config_char = config_char & (color_txt_flags | color_bg_flags);
    uint16_t final_car = 0;
    final_car = config_char << 8;
    final_car = final_car | c;
    *case_mem = final_car;
}*/

void efface_ecran(void) {
    for (int i = 0; i < H_VIDEO; i++) {
        for (int j = 0; j < W_VIDEO; j++) {
            ecrit_car(i, j, ' ', 15, 0);
        }
    }
}

void place_curseur(uint32_t lig, uint32_t col) {
    uint16_t pos = col + lig * 80;
    uint8_t down = (uint8_t) pos;
    uint8_t up = (uint8_t) (pos >> 8);
    outb(DOWN_CMD, CMD_CUR);
    outb(down, DATA_CUR);
    outb(UP_CMD, CMD_CUR);
    outb(up, DATA_CUR);
}

void erase_line(uint32_t lig) {
    for (int i = 0; i < W_VIDEO; i++) {
        ecrit_car(lig, i, ' ', 15, 0);
    }
}

void defilement(void) {
    uint16_t *memory = (uint16_t *) MEM_VIDEO;
    memmove(memory, memory + W_VIDEO, (H_VIDEO * W_VIDEO) * 2);
    erase_line(H_VIDEO - 1);
    cur_y--;
    place_curseur(cur_y, cur_x);
}

void traite_car(char c) {
    switch (c) {
        case 8:
            if (cur_x > 0) cur_x--;
            break;
        case 9:
            cur_x = (cur_x & 0xF8) + 8;
            if (cur_x == 80) cur_x = 79;
            break;
        case 10:
            cur_y++;
            cur_x = 0;
            break;
        case 12:
            efface_ecran();
            cur_y = 0;
            cur_x = 0;
            break;
        case 13:
            cur_x = 0;
            break;
        default:
            ecrit_car(cur_y, cur_x, c, col_txt, col_bg);
            cur_y = cur_y + (cur_x / (W_VIDEO - 1));
            cur_x = (cur_x + 1) % W_VIDEO;
            break;
    }
    if (cur_y == H_VIDEO) defilement();
    place_curseur(cur_y, cur_x);
}

void console_putbytes(const char *chaine, int taille) {
    for (int i = 0; i < taille; i++) {
        traite_car(chaine[i]);
        outb((unsigned char) chaine[i], 0xE9);
    }
}

void console_print_keyboard(const char *chaine, int taille) {
    for (int i = 0; i < taille; i++) {
        if (chaine[i] == 9 || (chaine[i] >= 32 && chaine[i] <= 126))
            traite_car(chaine[i]);
        else if (chaine[i] == 13)
            traite_car(10);
        else if (chaine[i] == 127) {
            traite_car(8);
            traite_car(32);
            traite_car(8);
        } else if (chaine[i] < 32) {
            traite_car('^');
            traite_car((char) (chaine[i] + 64));
        }
        outb((unsigned char) chaine[i], 0xE9);
    }
}

void cons_echo(int on) {
    cons_echo_bool = (unsigned int) on;
}

void print_top_right(char *string, int32_t size) {
    for (int i = 0; i < size; i++) {
        ecrit_car(0, W_VIDEO - size + i, string[i], 15, 0);
    }
}

void print_top_left(char *string, int32_t size) {
    for (int i = 0; i < size; i++) {
        ecrit_car(0, i, string[i], 15, 0);
    }
}

void keyboard_handler() {
    outb(0x20, 0x20);
    char in = inb(0x60);
    do_scancode(in);
}

void keyboard_data(char *str) {
    int len = strlen(str);
    process_t *waiting_p;

    if (cons_echo_bool)
        console_print_keyboard(str, (int) len);
    for (int i = 0; i < len; i++) {
        if (str[i] == 127 && keyboard_buffer_index > 0) {
            keyboard_buffer_index--;
        } else if (keyboard_buffer_index < sizeof(keyboard_buffer)) {
            keyboard_buffer[keyboard_buffer_index] = str[i];
            keyboard_buffer_index++;
			
        }
        if (str[i] == 13) {
            waiting_p = process_pop_waiting(p);
            if (waiting_p != NULL) {
                waiting_p->state = READY;
                schedule();
            }
        }
    }
	if ( keyboard_buffer_index > 0 )
	{
		waiting_p = process_pop_waiting_key();
		if (waiting_p != NULL) {
			waiting_p->state = READY;
			schedule();
		}
	}
}

void remove_waiting_proc(process_t *process) {
	if (process->l_wait_io.prev != NULL)
		queue_del(process, l_wait_io);
	if (process->l_wait_io_key.prev != NULL)
		queue_del(process, l_wait_io_key);
}

void get_key(int *key) {
    elected->state = WAIT_IO;
    process_add_waiting_key(elected);
    schedule();
	assert(keyboard_buffer_index > 0);
	if (key != NULL)
	{
		*key = (int)keyboard_buffer[keyboard_buffer_index-1];
	}
    memcpy(keyboard_buffer, &keyboard_buffer[1], sizeof(keyboard_buffer) - 2);
	keyboard_buffer_index--;
}

unsigned long cons_read(char *string, unsigned long length) {
    if (length == 0) return 0;
    elected->state = WAIT_IO;
    process_add_waiting(elected);
    schedule();
    uint32_t i;
    for (i = 0; i < keyboard_buffer_index && i < length && keyboard_buffer[i] != 13; i++)
        string[i] = keyboard_buffer[i];
    if (i < sizeof(keyboard_buffer) - 1) {
        memcpy(keyboard_buffer, &keyboard_buffer[i + 1], sizeof(keyboard_buffer) - i - 1);
        keyboard_buffer_index -= i + 1;
    } else {
        keyboard_buffer_index = 0;
        memset(keyboard_buffer, 0, sizeof(keyboard_buffer));
    }
    return (unsigned long) i;
}
