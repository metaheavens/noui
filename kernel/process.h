#ifndef _PROCESS_H
#define _PROCESS_H

#include "../shared/queue.h"
#include "../shared/stdint.h"

int start(int (*pt_func)(void *), unsigned long ssize, int prio, const char *name, void *arg);
int start_user(int (*pt_func)(void *), unsigned long ssize, int prio, const char *name, void *arg);

void process_init(uint32_t pid, char *name, uint32_t priority);

void schedule_handler(void);

#define KERNEL_STACK_SIZE 1024

typedef enum p_state {
    ELECTED,
    READY,
    WAIT_SEM,
    WAIT_IO,
    WAIT_MSG,
    WAIT_CHILD,
    ASLEEP,
    ZOMBIE,
    KILLED
} p_state_e;

typedef struct process {
    int32_t pid;
    char pname[64];
    p_state_e state;
    uint32_t store_register[5];
    uint32_t *stack;
    uint32_t kernel_stack[KERNEL_STACK_SIZE];
    int32_t priority;
    uint32_t stack_size;
    link l;
    link l_child;
    link l_wait_io;
    link l_wait_io_key;
    unsigned long wakeuptime;
    struct list_link *children;
    struct process *father;
    int32_t retval;
    link wait_read;
    link wait_write;
} process_t;

void update_sleep_time();

/*
 * Public API
 */
process_t *elected;

void switch_process(process_t *new);

int getpid(void);

char *elected_pname(void);

int chprio(int pid, int newprio);

void wait_clock(unsigned long time);

void exit_p(int retval);

int kill(int pid);

void top();

void ps();

void schedule(void);

int waitpid(int pid, int *retvalp);

int getprio(int pid);

#endif
