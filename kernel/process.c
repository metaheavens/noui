#include "process.h"
#include "interupt.h"
#include "../shared/const.h"
#include "mem.h"
#include "../shared/string.h"
#include "../shared/printf.h"
#include "cpu.h"
#include "../shared/stdint.h"
#include "../shared/stddef.h"
#include "../shared/stdbool.h"
#include "messages.h"
#include "../user/start.h"
#include "ecran.h"
#include "user_stack_mem.h"

extern void ctx_sw(void *, void *);

extern void exit_normally();
extern void start_proc(uint32_t *kernel_stack_pointer);
extern long totalsec;
extern unsigned long totalclock;

#ifndef NBPROC
#define NBPROC 1024
#endif

#ifndef MAXPRIO
#define MAXPRIO 256
#endif

/**
 * Macros to manage process and children efficiently
 */

#define process_add(p) queue_add(p, &process_queue, process_t, l, priority)
#define process_pop() queue_out(&process_queue, process_t, l)
#define process_top() queue_top(&process_queue, process_t, l)
#define process_del(ptr) queue_del(ptr, l)
#define process_for_each(ptr) queue_for_each_prev(ptr, &process_queue, process_t, l)

#define children_for_each(ptr, father) queue_for_each(ptr, father->children, process_t, l_child)
#define children_add(p, father) queue_add(p, father->children, process_t, l_child, pid)
#define children_top(father) queue_top(father->children, process_t, l_child)
#define children_del(ptr) queue_del(ptr, l_child)

/**
 * End of macros
 */

/**
 * Struct that is used to store a free pid. There are stored in a queue.
 */
typedef struct freepid {
    int32_t pid;
    link l;
    int32_t priority;
} freepid_t;

/**
 * Macros for manage free pids
 */
#define freepid_add(p) queue_add(p, &freepid_queue, freepid_t, l, priority)
#define freepid_pop() queue_out(&freepid_queue, freepid_t, l)


/**
 * Initialization of free pid queue and first pid
 */
LIST_HEAD(freepid_queue);

process_t *init;

uint32_t next_pid = 1;

LIST_HEAD(process_queue);


const long time_to_change = CLOCKFREQ / SCHEDFREQ;
long time_passed = 0;

/**
 * State that indicate if there is process to remove.
 */
bool has_process_to_kill = false;

char map_pstate_string[][64] = {"ELECTED", "READY", "WAIT_SEM", "WAIT_IO", "WAIT_MSG", "WAIT_CHILD", "ASLEEP", "ZOMBIE", "KILLED"};

void show_state(process_t *ptr) {
    printf("pid: %d wakeup: %ld totalsec: %ld prio: %d state: %s\n", ptr->pid, ptr->wakeuptime, totalsec, ptr->priority, map_pstate_string[ptr->state]);
}

/**
 * Switch between the actual elected process and the new process.
 *
 * @param new Process that you want to switch on.
 */
void switch_process(process_t *new) {
    process_del(new);
    // Save currently actual process
    process_t *actual = elected;
    if (actual->state == ELECTED) actual->state = READY;
    elected = new;
    // Pop new process and switch to it
    process_add(actual);
    assert(elected->state == READY);
    elected->state = ELECTED;

    uint32_t *tss_stack_kernel = (uint32_t *) 0x20004;
    *tss_stack_kernel = (uint32_t) &(elected->kernel_stack[KERNEL_STACK_SIZE - 1]);

    ctx_sw(actual->store_register, elected->store_register);
}

/**
 * Kill all processes that are marked "KILLED" in the process list. It is useful to clean process that killed himself,
 * when we can't remove it directly.
 */
void kill_tokill(void) {
    process_t *prev = NULL;
    process_t *ptr = NULL;
    queue_for_each(ptr, &process_queue, process_t, l) {
        if (ptr->state == KILLED) {
            process_del(ptr);

            /*
             * Add pid to free pid
             */
            freepid_t *f = mem_alloc(sizeof(freepid_t));
            f->priority = 0;
            f->pid = ptr->pid;
            freepid_add(f);
#ifdef DEV
            printf("pid is released: %d\n", f->pid);
#endif
            /*
             * Free process memory
             */
            user_stack_free(ptr->stack, ptr->stack_size);
            mem_free(ptr->children, sizeof(link));
            mem_free(ptr, sizeof(process_t));

            ptr = prev;
            if (prev == NULL) {
                ptr = process_top();
                if (ptr == NULL) break;
            }
        } else {
            prev = ptr;
        }
    }
    has_process_to_kill = false;
}

/**
 * Main schedule function. Look at the other process to find if there is more prioritary ready process.
 * It also change to another process with the same prio if the quantum of time is passed.
 */
void schedule(void) {
    update_sleep_time();
    if (has_process_to_kill) kill_tokill();

    process_t *actual = NULL;

    process_for_each(actual) {
        if (actual->state == READY) {
            if (elected->state != ELECTED) {
                switch_process(actual);
                return;
            } else if (actual->priority > elected->priority) {
                time_passed = 0;
                switch_process(actual);
                return;
            } else if (time_passed == 0 && actual->priority == elected->priority) {
                switch_process(actual);
                return;
            }
        }
    }

    if (elected->state != ELECTED) {
        printf("Les carottes sont cuites\n\n");
        printf("                 - Bambou\n");
    }
}

void init_children(process_t *p, process_t *father) {
    p->children = mem_alloc(sizeof(link));
    p->children->next = p->children;
    p->children->prev = p->children;
    p->father = father;
}

int start_user(int (*pt_func)(void *), unsigned long ssize, int prio, const char *name, void *arg) {
    if(name < (const char *) 0x1000000) return -1;
    return start(pt_func, ssize, prio, name, arg);
}

int start(int (*pt_func)(void *), unsigned long ssize, int prio, const char *name, void *arg) {
    if (prio <= 0 || prio > MAXPRIO)
        return -1;

    if (ssize > 8192) return -1;
    //ssize is given in bytes, check it
    uint32_t total_bytes_stack = (sizeof(int8_t) * ssize) + sizeof(arg) + (7 * sizeof(int *));

    uint32_t modulo = total_bytes_stack % 4;

    assert(modulo == 0);
    int stack_size = total_bytes_stack / 4;

    freepid_t *f = freepid_pop();
    int32_t pid = next_pid;
    if (f == NULL) {
        if (next_pid < NBPROC) {
            next_pid++;
        } else {
            return -1;
        }
    } else {
#ifdef DEV
        printf("TAKING A PID: %d\n", f->pid);
#endif
        pid = f->pid;
        mem_free(f, sizeof(freepid_t));
    }
    process_t *p = mem_alloc(sizeof(process_t));

    // p->taille_pile = ssize*sizeof(int *)+sizeof(arg)+2*sizeof(int *);

    p->stack_size = total_bytes_stack;
    p->stack = user_stack_alloc(p->stack_size);
    p->pid = pid;
    strncpy(p->pname, name, sizeof(p->pname));
    p->state = READY;

    //Setup %esp saved register to the head of the stack
    p->store_register[1] = (uint32_t) &(p->kernel_stack[KERNEL_STACK_SIZE - 6]);
    //@ of function proc1 put in head of the stack
    p->stack[stack_size - 1] = (uint32_t) arg;
    p->stack[stack_size - 2] = (uint32_t) &user_start;

    /**
     * Init of the kernel stack
     */
    p->kernel_stack[KERNEL_STACK_SIZE - 1] = (uint32_t) 0x4b;
    p->kernel_stack[KERNEL_STACK_SIZE - 2] = (uint32_t) &(p->stack[stack_size - 2]);
    p->kernel_stack[KERNEL_STACK_SIZE - 3] = (uint32_t) 0x202;
    p->kernel_stack[KERNEL_STACK_SIZE - 4] = (uint32_t) 0x43;
    p->kernel_stack[KERNEL_STACK_SIZE - 5] = (uint32_t) pt_func;
    p->kernel_stack[KERNEL_STACK_SIZE - 6] = (uint32_t) start_proc;

    /**
     * Init of TSS
     */
    uint32_t *tss_segment = (uint32_t *) 0x20008;
    *tss_segment = 0x18;

    p->priority = prio;

    init_children(p, elected);

    children_add(p, elected);

    process_add(p);

    if (p->priority > elected->priority)
        schedule();
    return p->pid;
}

/*
 * Handler that is ran by the TIC interruption.
 */
void schedule_handler(void) {
    time_passed++;
    if (time_passed == time_to_change) {
        time_passed = 0;
        schedule();
    }
}

/**
 * Check if someone have to be waked up according to his wake up time.
 */
void update_sleep_time() {
    process_t *ptr;
    process_for_each(ptr) {
        if (ptr->wakeuptime <= totalclock && ptr->state == ASLEEP) {
            ptr->state = READY;
            ptr->wakeuptime = 0;
        }
    }
}

/**
 * Init the first process that will run if there is no other process to run.
 */
void process_init(uint32_t pid, char *name, uint32_t priority) {
    elected = (process_t *) mem_alloc(sizeof(process_t));
    elected->pid = pid;
    strcpy(elected->pname, name);
    elected->state = ELECTED;
    elected->priority = priority;

    init_children(elected, NULL);

    init = elected;
}

/**
 * Function used by top to recursivly show the state of processes in a beautiful tree.
 */
void rec_top(process_t *actual, int depth) {
    for (int i = 0; i < depth; i++) {
        printf(" |");
    }
    printf(" -- %s %d %s\n", actual->pname, actual->pid, map_pstate_string[actual->state]);

    process_t *child = NULL;
    children_for_each(child, actual) {
        rec_top(child, depth + 1);
    }
}

/*
 * PUBLIC API
 */

int getpid(void) {
    assert(elected != NULL);
    return elected->pid;
}

char *elected_pname(void) {
    assert(elected != NULL);
    return elected->pname;
}

int chprio(int pid, int newprio) {
    if (newprio <= 0 || newprio > MAXPRIO)
        return -1;

    process_t *ptr;
    if (elected->pid == pid) {
        if (elected->state == ZOMBIE)
            return -1;
        int32_t oldprio = elected->priority;
        elected->priority = newprio;

        schedule();
        return oldprio;
    }
    process_for_each(ptr) {
        if (ptr->pid == pid) {
            if (ptr->state == ZOMBIE)
                return -1;
            int32_t oldprio = ptr->priority;
            ptr->priority = newprio;

            process_del(ptr);
            process_add(ptr);

            schedule();
            return oldprio;
        }
    }
    return -1;
}

void clean_children(process_t *p) {
    process_t *actual = NULL;
    children_for_each(actual, p) {
        actual->father = NULL;
        if (actual->state == ZOMBIE) {
            actual->state = KILLED;
            has_process_to_kill = true;
        }
    }
}

/**
 * Try to kill a process. If it has a father, it will put it in zombie state.
 */
void kill_or_zombie(process_t *p, int retval) {
    p->retval = retval;
    if(p->wait_read.next != NULL && p->wait_read.prev != NULL) queue_del(p, wait_read);
    if(p->wait_write.next != NULL && p->wait_write.prev != NULL) queue_del(p, wait_write);
    if (p->father != NULL) {
        p->state = ZOMBIE;
        if (p->father->state == WAIT_CHILD) p->father->state = READY;
    } else {
        p->state = KILLED;
        has_process_to_kill = true;
    }
    clean_children(p);
}

void exit_p(int retval) {
    assert(getpid() != 0);
#ifdef DEV
    printf("*******\n");
    show_state(elected);
    printf("*******\n");
    printf("I exited with return code %d\n", retval);
#endif
    kill_or_zombie(elected, retval);
    schedule();
}

int kill(int pid) {
    if (pid == 0) return -1;
    process_t *ptr = NULL;
    if (elected->pid == pid) {
		if (elected->state == WAIT_IO)
			remove_waiting_proc(elected);
        if (elected->state == ZOMBIE) return -1;
        kill_or_zombie(elected, 0);
        schedule();
        return 0;
    } else {
        process_for_each(ptr) {
            if (ptr->pid == pid) {
				if (ptr->state == WAIT_IO)
					remove_waiting_proc(ptr);
                if (ptr->state == ZOMBIE) return -1;
                kill_or_zombie(ptr, 0);
                return 0;
            }
        }
    }
    return -1;
}

void wait_clock(unsigned long time) {
    elected->state = ASLEEP;
    elected->wakeuptime = time;
    schedule();
}

void top() {
    process_t *actual = init;
    rec_top(actual, 0);
}

void ps() {
    printf("->");
	show_state(elected);
    process_t *actual;
    process_for_each(actual) {
        show_state(actual);
    }
}

int waitpid(int pid, int *retvalp) {
    if (retvalp > ((int *) 0) && retvalp < (int *) 0x1000000) return -1;
    process_t *actual = NULL;

    if (pid < 0) {
        process_t *top = children_top(elected);
        if (top == NULL) return -1;
        for (;;) {
            children_for_each(actual, elected) {
                if (actual->state == ZOMBIE) {
                    if (retvalp != NULL) *retvalp = actual->retval;
                    actual->state = KILLED;
                    //has_process_to_kill = true;
                    children_del(actual);
                    int oldpid = actual->pid;
                    kill_tokill();
                    return oldpid;
                }
            }
            elected->state = WAIT_CHILD;
            schedule();
        }
    } else {
        process_t *top = children_top(elected);
        if (top == NULL) return -1;
        children_for_each(actual, elected) {
            if (actual->pid == pid) {
                for (;;) {
                    if (actual->state == ZOMBIE) {
                        if (retvalp != NULL) *retvalp = actual->retval;
                        actual->state = KILLED;
                        //has_process_to_kill = true;
                        children_del(actual);
                        int oldpid = actual->pid;
                        kill_tokill();
                        return oldpid;
                    } else {
                        elected->state = WAIT_CHILD;
                        schedule();
                    }
                }
            }
        }
        return -1;
    }
}

int getprio(int pid) {
    process_t *ptr = NULL;
    if (elected->pid == pid) return elected->priority;
    process_for_each(ptr) {
        if (ptr->pid == pid) return ptr->priority;
    }
    return -1;
}
