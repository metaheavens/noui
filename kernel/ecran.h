#ifndef CR_ECRAN_H
#define CR_ECRAN_H

#include "stdint.h"
#include "process.h"

extern uint16_t *ptr_mem(uint32_t lig, uint32_t col);
extern void ecrit_car(uint32_t lig, uint32_t col, char c, uint32_t coul_texte, uint32_t coul_fond);
extern void efface_ecran(void);
extern void place_curseur(uint32_t lig, uint32_t col);
extern void traite_car(char c);
extern void defilement(void);
void console_putbytes(const char *chaine, int taille);
void cons_echo(int on);
extern void print_top_right(char *string, int32_t size);
extern void print_top_left(char *string, int32_t size);
void keyboard_handler();
void get_key(int *key);
unsigned long cons_read(char *string, unsigned long length);
void remove_waiting_proc(process_t *process);
#endif
